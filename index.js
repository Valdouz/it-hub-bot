const Discord = require("discord.js")
const Canvas = require("canvas")
const fs = require("fs")
const logs = require("discord-logs")
const db = require("quick.db")
const moment = require("moment")
const schedule = require("node-schedule")
let xp = new Set()
const ms = require('ms')
const AntiSpam = require('discord-anti-spam')
const antiSpam = new AntiSpam({
    warnThreshold: 3,
    kickThreshold: 7,
    banThreshold: 7,
    maxInterval: 2000,
    maxDuplicatesInterval:120000,
    warnMessage: '{@user}, merci d\'arrêter ce spam.',
    kickMessage: '**{user_tag}** a été exclu pour spam.',
    banMessage: '**{user_tag}** a été banni pour spam.',
    maxDuplicatesWarning: 3,
    maxDuplicatesKick: 5,
    maxDuplicatesBan: 7,
    exemptPermissions: [ 'ADMINISTRATOR'],
    ignoreBots: true,
    verbose: true,
    ignoredRoles:[require('./settings.json').staffRoleId]
});

if (!require('./settings.json').licence) {
  console.log(`
    --------------------------------
    In order to run the code, you
    must accept the licence, by
    setting licence to true in the
    config.json file that you have created.
    --------------------------------
    Pour executer le code, vous devez
    accepter les termes de la licence
    en mettant licence en true dans
    le fichier config.json cree.`)

    return;
} else {
  console.log(`
    --------------------------------
    By running this code, you accept
    the licence terms, that you can
    find in the licence/licence.txt
    file, in the main directory
    --------------------------------
    En executant ce code, vous acceptez
    les termes de la licence trouvable
    dans le fichier licence.txt du
    dossier licence a la racine du projet`)
}

let client = new Discord.Client({ partials: ["REACTION", "MESSAGE"]})
client.settings = require('./settings.json')
client.login(require("./settings.json").token)
client.commands = new Discord.Collection();
client.aliases = new Discord.Collection();
client.categories = fs.readdirSync("./commands/");

client.queue = new Map();

client.functions = require("./functions.js")
client.logs = require("./logs.js")

const TempChannels = require("discord-temp-channels");
const tempChannels = new TempChannels(client);

tempChannels.registerChannel(client.settings.tempChannel.channel, {
    childCategory: client.settings.tempChannel.category,
    childAutoDelete: true,
    childMaxUsers: 99,
    childFormat: (member, count) => `#${count} | ${member.user.username}`
});

tempChannels.on("childCreate", (member, channel, parentChannel) => {
    channel.createOverwrite(member.user, {
        MANAGE_CHANNELS: true
    })
})
client.invisible = "#000"

const constant = require("./node_modules/discord.js/src/util/Constants")
constant.DefaultOptions.ws.properties.$browser = 'Discord iOS'

let invites = {}
client.invites = invites

client.on("ready", () => {

    console.log(client.user.username + ' est en ligne !')

    fs.readdirSync("./commands/").forEach(dir => {
        const commands = fs.readdirSync(`./commands/${dir}/`).filter(file => file.endsWith(".js"));

        for (let file of commands) {
            let pull = require(`./commands/${dir}/${file}`);

            if (pull.name) {
                client.commands.set(pull.name, pull);
            } else {
                continue;
            }

            if (pull.aliases && Array.isArray(pull.aliases)) pull.aliases.forEach(alias => client.aliases.set(alias, pull.name));
        }
    });



    fs.readdir("./logs/", (err, files) => {
      if (err) return console.error(err);
      files.forEach(file => {
        if (!file.endsWith('.js')) return;
        const event = require(`./logs/${file}`);
        let eventName = file.split(".")[0];
        client.on(eventName, event.bind(null, client));
      });
    });

    client.guilds.cache.forEach(g => {
      g.fetchInvites().then(guildInvites => {
        invites[g.id] = guildInvites;
      });
    });

    logs(client)

    client.modules = {
        canvas: require("canvas")
    }

    client.functions.checkSupports(client)

    schedule.scheduleJob('00 * * * *', async function() {
      client.functions.checkSupports(client)
    })

    schedule.scheduleJob('0 00 * * *', async function(){
      client.guilds.cache.forEach(async guild => {
        let stats = await client.functions.postStats(client, guild.id)

        const channel = guild.channels.cache.get(client.settings.statsChannelId) //guild.channels.cache.get(db.get(`stats_${guild.id}.chanid`))

        db.set(`stats_${guild.id}`, {})

        if(!channel) return;

        channel.send(stats)

        let memberHistory = db.get(`members.${guild.id}`);

        for (i in memberHistory) {
          memberHistory[i] = memberHistory[i+1]
        };

        memberHistory[6] = guild.memberCount;

        if (!db.get(`members`)) db.set(`members`, {});
        if (!db.get(`members.${guild.id}`)) db.set(`members.${guild.id}`, []);

        db.set(`members.${guild.id}`, memberHistory);
      })
    })

    setInterval(function() {
        let activities = [
            `{num} geeks`,
            `Qwant, le moteur de recherche qui respecte la vie privée (et 100% fr)`
        ]
        client.user.setActivity(activities[Math.floor(Math.random() * activities.length)].replace('{num}', client.guilds.cache.get(client.settings.mainGuildId).memberCount), { type: "WATCHING"})
    }, 10000);

    if (!db.get('bump')) db.set('bump', {})
    for (i in db.get(`bump`)) {
      let bump = db.get(`bump[${i}]`)
      let now = Date.now();

      let lastBump = bump.date

      let diff = now-lastBump;

      let role = client.guilds.cache.get(client.settings.mainGuildId).roles.cache.get(client.settings.promotionRoleId);

      if (!role) return;

      let user = client.users.cache.get(bump.id)
      if (!user) return;

      let member = client.guilds.cache.get(client.settings.mainGuildId).member(user)
      if (!member) return;

      if (diff >= ms('12h')) {
        member.roles.remove(role);
      } else {
        setTimeout(function() {
          member.roles.remove(role)
        }, (ms('12h')-diff))
      }
    }

    for (i in db.get(`tempmutes`)) {
      let muteInfo = db.get(`tempmutes.${i}`);

      console.log(i)

      if (!muteInfo) return;

      let timeout = muteInfo.timeout;

      if (timeout <= Date.now()) {
        let user = client.guilds.cache.get(muteInfo.guildId).member(client.users.cache.get(muteInfo.id));

        if (!user) return;

        let role = client.guilds.cache.get(muteInfo.guildId).roles.cache.get(client.settings.muteRole);

        if (!user.roles.cache.has(client.settings.muteRole)) return;

        user.roles.remove(role);

        let logs = client.channels.cache.get(client.settings.logsChannelId);
        if (logs) {
          logs.send(new Discord.MessageEmbed().setColor('GREEN').setDescription(`
            **Fin de mute**

            Utilisateur : \`${user.user.tag}\``))
        }

        db.set(`tempmutes.${user.user.id}`, null)
      } else {
        let diff = timeout-Date.now();

        setTimeout(function() {
          let user = client.guilds.cache.get(muteInfo.guildId).member(client.users.cache.get(muteInfo.id));

          if (!user) return;

          let role = client.guilds.cache.get(muteInfo,guildId).roles.cache.get(client.settings.muteRole);

          if (!user.roles.cache.has(client.settings.muteRole)) return;

          user.roles.remove(role);

          let logs = client.channels.cache.get(client.settings.logsChannelId);
          if (logs) {
            logs.send(new Discord.MessageEmbed().setColor('GREEN').setDescription(`
              **Fin de mute**

              Utilisateur : \`${user.user.tag}\``))
          }

          db.set(`tempmutes.${user.user.id}`, null)
        }, diff)
      }
    }
})

client.on('stats', async () => {
  client.guilds.cache.forEach(async guild => {
    let stats = await client.functions.postStats(client, guild.id)

    const channel = guild.channels.cache.get(client.settings.statsChannelId)

    db.set(`stats_${guild.id}`, {})

    if(!channel) return;

    channel.send(stats)
  })
})

client.on("message", async message => {

    antiSpam.message(message)

    if (message.author.bot) return;

    if (!db.get(`stats_${message.guild.id}.channels`)) db.set(`stats_${message.guild.id}.channels`, {});
    if (!db.get(`stats_${message.guild.id}.users`)) db.set(`stats_${message.guild.id}.users`, {count:0,ids:{}})

    db.set(`stats_${message.guild.id}.messages`, (db.get(`stats_${message.guild.id}.messages`))+1)
    let time = new Date().getHours()
    db.set(`stats_${message.guild.id}.hourMessages.${time}`, (db.get(`stats_${message.guild.id}.hourMessages.${time}`))+1)
    db.set(`stats_${message.guild.id}.channels[${message.channel.id}]`, (db.get(`stats_${message.guild.id}.channels[${message.channel.id}]`))+1)
    if(!db.get(`stats_${message.guild.id}.users.ids[${message.author.id}]`)) {
      db.set(`stats_${message.guild.id}.users.count`, (db.get(`stats_${message.guild.id}.users.count`))+1)
    }
    db.set(`stats_${message.guild.id}.users.ids[${message.author.id}]`, {id:message.author.id, count:Math.round(db.get(`stats_${message.guild.id}.users.ids[${message.author.id}].count`) || 0)+1} )


    const prefix = client.settings.prefix

    if(!message.content.startsWith(prefix)) {
      if (!client.settings.xp) return;
      var localxp = db.get(`xp_${message.guild.id}`)

      if(xp.has(message.author.id)) return;

      var expGet = Math.floor(Math.random() * (25 - 15 + 1) + 15);
      let actual = db.get(`xp_${message.guild.id}.${message.author.id}.xp`) || 0
      let level = db.get(`xp_${message.guild.id}.${message.author.id}.lvl`) || 1
      let expForLevel = 5 * (Math.pow(level, 2)) + 50 * level + 100;
      actual = actual + expGet

      for (i in client.settings.levelRoles) {
        let role = message.guild.roles.cache.get(client.settings.levelRoles[i]);

        if (!role) return;

        if (level >= i) {
          message.member.roles.add(role)
        }
      }

      if (actual > expForLevel) {
          actual = 0
          level++


          if (client.settings.levelRoles[level]) {
            let role = message.guild.roles.cache.get(client.settings.levelRoles[level]);

            message.member.roles.add(role);

            message.channel.send(`:tada: | Bravo ${message.author}, vous venez de passer au **niveau ${level}**, et vous obtenez le rôle \`${client.guilds.cache.get(client.settings.mainGuildId).roles.cache.get(client.settings.levelRoles[level]).name}\` !`);
          } else {
            message.channel.send(`:tada: | Bravo ${message.author}, vous venez de passer au **niveau ${level}** !`);
          }
      }

      db.set(`xp_${message.guild.id}.${message.author.id}.xp`, actual)
      db.set(`xp_${message.guild.id}.${message.author.id}.lvl`, level)

      xp.add(message.author.id)

      setTimeout(function() {
          xp.delete(message.author.id)
      }, 30000)
    } else {

          const args = message.content.slice(prefix.length).trim().split(/ +/g);
          const cmd = args.shift().toLowerCase();
          if (cmd.length === 0) return;
          let command = client.commands.get(cmd);
          if (!command) command = client.commands.get(client.aliases.get(cmd));
          if (command)  {
            command.run(client, message, args)
          }
          /* if (message.content.startsWith('!d bump')) {
            setTimeout(function() {
              let msg = client.channels.cache.get(message.channel.id).lastMessage;

              if (msg.author.id == message.author.id) return;

              let validBumpEmbedColor = '2406327'

              if (!msg.embeds) return;

              if (msg.embeds[0].color != validBumpEmbedColor) return;
              let role = message.guild.roles.cache.get(client.settings.promotionRoleId);

              if (!role) return message.channel.send(new Discord.MessageEmbed().setColor('RED').setDescription(':x: | L\'identifiant du rôle de promotion est invalide. Veuillez le vérifier dans le fichier settings.json.'))

              message.member.roles.add(role);

              message.channel.send(new Discord.MessageEmbed().setColor('GREEN').setDescription(':white_check_mark: | Merci ! En bumpant le serveur vous nous permettez de nous agrandir, pour vous remercier vous recevez automatiquement le rôle promoteur pour les 12 prochaines heures.'))

              if (!db.get('bump')) db.set('bump', {})
              db.set(`bump[${message.author.id}]`, {date: Date.now(), id: message.author.id})
              setTimeout(function() {
                let now = Date.now();
                let diff = now-db.get(`bump[${message.author.id}].date`);

                if (diff >= ms('12h')) {
                  let invites = db.get(`invites_${message.guild.id}.${message.author.id}`) || {};
                  let total = (invites.true || 0)+(invites.bonus || 0)-(invites.fake || 0);

                  let hasNoRole = true;

                  for (i in client.settings.invites) {
                    if (total >= i) {
                      if (client.settings.invites[i] == role) hasNoRole = false;
                    }
                  }

                  if (!hasNoRole) return;

                  message.member.roles.remove(role)
                }
              }, ms('12h'))
            }, 1000)
          }*/
    }
})

client.on("guildMemberAdd", async member =>  {

    db.set(`stats_${member.guild.id}.joins`, (db.get(`stats_${member.guild.id}.joins`) || 0)+1)

    const channel = client.channels.cache.get(client.settings.welcomeChannelId)

    channel.send(`Bienvenue ${member} ! **N'hésite pas à aller choisir tes rôles dans <#783118421038333993> !**`, await client.functions.welcomeImage(member.user, Canvas, Discord, member.guild))

    client.functions.dbMemberAdd(member, invites, db, client)

    if (db.get(`tempmutes.${member.user.id}`)) {
      let muteInfo = db.get(`tempmutes.${member.user.id}`);

      if (!muteInfo) return;

      let timeout = muteInfo.timeout;

      if (timeout <= Date.now()) return;

      let role = client.guilds.cache.get(muteInfo.guildId).roles.cache.get(client.settings.muteRole);

      member.roles.add(role)

      let diff = timeout-Date.now();

      setTimeout(function() {
        let user = client.guilds.cache.get(muteInfo.guildId).member(client.users.cache.get(muteInfo.id));

        if (!user) return;

        if (!user.roles.cache.has(client.settings.muteRole)) return;

        user.roles.remove(role);

        let logs = client.channels.cache.get(client.settings.logsChannelId);
        if (logs) {
          logs.send(new Discord.MessageEmbed().setColor('GREEN').setDescription(`
            **Fin de mute**

            Utilisateur : \`${user.user.tag}\``))
        }

        db.set(`tempmutes.${user.user.id}`, null)
      }, diff)
    }
})

client.on('guildMemberRemove', async member => {

db.set(`stats_${member.guild.id}.leaves`, (db.get(`stats_${member.guild.id}.leaves`))+1)

    if (db.get(`supports.${member.user.id}`)) {
      let channel = client.channels.cache.get(db.get(`supports.${member.user.id}`));

      if (!channel) return;

      channel.delete()
    }

    for (i in client.settings.invites) {
      let role = member.guild.roles.cache.get(client.settings.invites[i]);

      if (!role) return;

      let inviter = client.users.cache.get(db.get(`inviter.${member.id}`));

      if (!inviter) return;

      let invites = db.get(`invites_${inviter.id}`) || {};

      let invitesTotal = (invites.true || 0) + (invites.bonus || 0) - (invites.fake || 0);

      let inviterMember = member.guild.member(inviter)

      if (!inviterMember) return;

      if (invitesTotal < i && inviterMember.roles.cache.has(role.id)) {

        inviterMember.roles.remove(role);

      }
    }
})

client.on("messageReactionAdd", async (messageReaction, user) => {

    if(messageReaction.message.partial) await messageReaction.message.fetch()

    if(messageReaction.partial) await messageReaction.fetch()

    async function getMember(user, rule) {
        const channel = await user.client.channels.fetch(rule.channelId);
        return channel.guild.members.fetch(user);
    }

    function isValidMessageReactionAdd(messageReaction, user) {
        if (user.bot) return false;

        if (messageReaction.message.channel.type !== 'text') return false;

        return true;
    }

    const rules = require("./data/reactionRoles.json")

    if (!isValidMessageReactionAdd(messageReaction, user)) return;

  const rule = rules[messageReaction.message.id];
  if (!rule) return;

  const emojiKey =
    messageReaction.emoji[messageReaction.emoji.id ? 'id' : 'name'];
  const roleIdsToAdd = rule.emojiRoleMap.role[emojiKey];
  if (!roleIdsToAdd) return;

  const member = await getMember(user, rule);
  if (!member) return;

  if (!rule.isUnique) {
    return await member.roles.add(roleIdsToAdd);
  }

  const currentRoleIds = member.roles.cache.map(role => role.id);
  const roleIdsToRemove = [...new Set(Object.values(rule.emojiRoleMap).flat())];
  const roleIdsToSet = [
    ...currentRoleIds.filter(roleId => !roleIdsToRemove.includes(roleId)),
    ...roleIdsToAdd
  ];

return await member.roles.set(roleIdsToSet);
})

client.on("messageReactionRemove", async (messageReaction, user) => {

    if(messageReaction.message.partial) await messageReaction.message.fetch()

    if(messageReaction.partial) await messageReaction.fetch()

    async function getMember(user, rule) {
        const channel = await user.client.channels.fetch(rule.channelId);
        return channel.guild.members.fetch(user);
    }

    function isValidMessageReactionAdd(messageReaction, user) {
        if (user.bot) return false;

        if (messageReaction.message.channel.type !== 'text') return false;

        return true;
    }

    const rules = require("./data/reactionRoles.json")

    if (!isValidMessageReactionAdd(messageReaction, user)) return console.log("invalid reaction");

  const rule = rules[messageReaction.message.id];
  if (!rule) return;

  const emojiKey =
    messageReaction.emoji[messageReaction.emoji.id ? 'id' : 'name'];
  const roleIdsToAdd = rule.emojiRoleMap.role[emojiKey];
  if (!roleIdsToAdd) return;

  const member = await getMember(user, rule);
  if (!member) return;

  //messageReaction.users.remove(user);

  if (roleIdsToAdd.every(roleId => member.roles.cache.has(roleId))) {
    return await member.roles.remove(roleIdsToAdd);
  }
});

/*
    This file is part of It-Hub Bot.

    It-Hub Bot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    It-Hub Bot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with It-Hub Bot.  If not, see <https://www.gnu.org/licenses/>.

*/
