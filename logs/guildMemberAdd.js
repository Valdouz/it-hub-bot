const Discord = require('discord.js')
const ms = require('ms')

module.exports = async (client, member) => {
  member.guild.fetchInvites().then(async guildInvites => {

    let invites = client.invites
    const ei = invites[member.guild.id];
    client.invites[member.guild.id] = guildInvites;
    const invite = guildInvites.find(i => !ei.get(i.code) || ei.get(i.code).uses < i.uses) || null
    let inviter;
    if(!invite) {
      inviter = {
        tag: 'Aucun inviteur identifié.'
      }
    } else {
      inviter = client.users.cache.get(invite.inviter.id);
    }

    let logs = client.channels.cache.get(client.settings.logsChannelId);

    if (!logs) return;

    let embed = new Discord.MessageEmbed().setColor('GREEN').setDescription(`
      **Nouveau membre :**

      Pseudo de l'utilisateur : \`${member.user.username}\`
      Compte créé il y a : \`${ms(ms((Date.now()-member.user.createdTimestamp).toString()))}\`
      Invité par : \`${inviter.tag}\``).setThumbnail(member.user.displayAvatarURL({}))
    .setTimestamp();

    logs.send(embed)
  })
}


/*
    This file is part of It-Hub Bot.

    It-Hub Bot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    It-Hub Bot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with It-Hub Bot.  If not, see <https://www.gnu.org/licenses/>.

*/
