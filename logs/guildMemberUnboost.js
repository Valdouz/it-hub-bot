const discord = require('discord.js');

module.exports = async (client, member) => {
  let logs = client.channels.cache.get(client.settings.logsChannelId);
  if (!logs) return;

  let embed = new Discord.MessageEmbed()
    .setColor('#1a356f')
    .setDescription(`
      **\`${member.user.tag}\` a arrêté de booster le serveur.**`)
    .setThumbnail(member.user.displayAvatarURL({}))
  .setTimestamp();

  logs.send(embed)
}


/*
    This file is part of It-Hub Bot.

    It-Hub Bot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    It-Hub Bot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with It-Hub Bot.  If not, see <https://www.gnu.org/licenses/>.

*/
