const Discord = require('discord.js')
const db = require('quick.db')
const ms = require('ms')

module.exports = {
  name: "customrole",
  aliases: ['cr'],
  category: "utility",
  run: async (client, message, args) => {
    if (!message.member.roles.cache.has(client.settings.boostRoleId)) return message.channel.send(new Discord.MessageEmbed().setColor('RED').setDescription(':x: | Seuls les boosters ont acces a cette commande !'))

    let role = {
        name: null,
        color: null,
        position: 21
    }

    async function createRole(editing) {

      let filter = (msg) => {
        return message.author.id === msg.author.id
      };

      const collector = message.channel.createMessageCollector(filter, { time: 30000 });

      message.channel.send(new Discord.MessageEmbed().setColor('#1f6996').setDescription('Quel nom souhaitez vous donner à votre rôle ?'));

      let step = 1
      collector.on('collect', async newMsg => {
        switch(step) {
          case 1:
            step++;
            role.name = newMsg.content
            newMsg.channel.send(new Discord.MessageEmbed().setColor('#1f6996').setDescription('Quelle couleur voulez vous donner à votre rôle (en code hexadecimal, générez le votre en cliquant [ici](https://www.color-hex.com/)).'));
          break;

          case 2:
            if (!newMsg.content.startsWith('#') || newMsg.content.lenght == 7) return message.channel.send(new Discord.MessageEmbed().setColor('RED').setDescription(':x: | Votre code hex est invalide. Veuillez réssayer.'))

            role.color = newMsg.content;

            let staffChannel = client.channels.cache.get(client.settings.staffChannelId);

            if (!staffChannel) return newMsg.channel.send(new Discord.MessageEmbed().setColor('RED').setDescription(':x: | Une erreur s\'est produite, merci de la signaler au developpeur.s'))

            newMsg.channel.send(new Discord.MessageEmbed().setColor(role.color).setDescription(':white_check_mark: | Votre demande a été prise en compte, un staff la validera sous peu.'));
            collector.stop()

            let valid = await staffChannel.send("<@&783118398354620486>", new Discord.MessageEmbed().setColor(role.color).setDescription(`:warning: | Demande de création de rôle personalisé boost par \`${message.author.username}#${message.author.discriminator}\`\nNom du role : \`${role.name}\`\nCouleur du role : \`${role.color}\`\nPour valider cette demande veuillez cliquer sur :white_check_mark: sous ce message.`));

            valid.react('✅');

            let filter = (reaction, user) => {
              return reaction.emoji.name === '✅'
            }

            let collector2 = valid.createReactionCollector(filter, { time: ms('12h') })

            collector2.on('collect', async (reaction, user) => {
              if (user.bot) return;
              if (!message.guild.member(user).roles.cache.has(client.settings.staffRoleId)) return;

              let creating = await staffChannel.send('Création du rôle en cours...');

              let customRole = await message.guild.roles.create({
                data:{
                  name: role.name,
                  color: role.color,
                  position: role.position,
                  mentionable: false
                }
              });

              message.member.roles.add(customRole);

              if (editing) {
                message.guild.roles.cache.get(db.get(`boosterRole.${message.author.id}`)).delete()
              }

              db.set(`boosterRole.${message.author.id}`, customRole.id)

              creating.edit(':white_check_mark: | La création et l\'ajout du rôle est terminé !')
            })

          break;
        }
      })
    }

    if (!db.get(`boosterRole`)) db.set(`boosterRole`, {})

    if (!db.get(`boosterRole.${message.author.id}`)) {
      createRole(false)
    } else {
      let choose = await message.channel.send(new Discord.MessageEmbed().setColor('#1f6996').setDescription('Que souhaitez vous faire ?\n\n⚙️ - Modifier votre role\n🗑️ - Supprimer votre role'));

      choose.react('⚙️');
      choose.react('🗑️');

      let filter = (reaction, user) => {
        return user.id === message.author.id
      }

      const collector = choose.createReactionCollector(filter, { time: 30000 });

      collector.on('collect', async (reaction, user) => {
        switch(reaction.emoji.name) {
          case '🗑️':
            let role = message.guild.roles.cache.get(db.get(`boosterRole.${message.author.id}`)).delete();

            db.set(`boosterRole.${message.author.id}`, null)

            message.channel.send(new Discord.MessageEmbed().setColor('#1f6996').setDescription(':white_check_mark: | Votre rôle personalisé a bien été supprimé !'));
          break;

          case '⚙️':
            createRole(true)
        }
      })
    }
  }
}


/*
    This file is part of It-Hub Bot.

    It-Hub Bot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    It-Hub Bot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with It-Hub Bot.  If not, see <https://www.gnu.org/licenses/>.

*/
