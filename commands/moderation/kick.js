const Discord = require('discord.js')
const db = require('quick.db')
const ms = require('ms')

module.exports = {
  name: "kick",
  aliases: ['k'],
  category: "moderation",
  run: async (client, message, args) => {
    if (!message.member.roles.cache.has(client.settings.modRoleId)) return message.channel.send(new Discord.MessageEmbed().setColor('#1a356f').setDescription(':x: | Vous n\'avez pas le role staff requis pour executer cela !'))

    let user = message.guild.member(message.mentions.users.first());
    if (!user) user = message.guild.members.cache.find(member => member.user.username == args[0]);
    if (!user) user = message.guild.members.cache.find(member => member.user.username.toLowerCase() == args[0]);
    if (!user) user = message.guild.members.cache.find(member => member.user.discriminator == args[0]);
    if (!user) user = message.guild.members.cache.find(member => member.user.tag == args[0]);
    if (!user) user = message.guild.members.cache.find(member => member.user.tag.toLowerCase() == args[0]);
    if (!user) user = message.guild.members.cache.find(member => member.user.id == args[0])

    if (!user) return message.channel.send(new Discord.MessageEmbed().setColor('#1a356f').setDescription(':x: | Je n\'ai pas trouvé cet utilisateur. Veuillez réessayer avec le pseudo, le tag, l\'identifiant, ou une mention.'));

    user.kick(`Expulse par ${message.author.tag} | ` + (args.join(' ') || 'Aucune raison fournie'))

    message.delete()
    message.channel.send(
      new Discord.MessageEmbed()
        .setColor('#1a356f')
        .setDescription(`:white_check_mark: | L'utilisateur \`${user.user.tag}\` a bien été expulsé !`)
    )

    let logs = client.channels.cache.get(client.settings.logsChannelId)
    if (!logs) return;

    logs.send(
      new Discord.MessageEmbed()
        .setColor('RED')
        .setDescription(`
          **Expulsion de membre :**

          Membre expulsé : \`${user.user.tag}\`
          Modérateur : \`${message.author.tag}\`
          Raison : \`${args.join(' ') || 'Aucune raison fournie'}\``)
    )
  }
}


/*
    This file is part of It-Hub Bot.

    It-Hub Bot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    It-Hub Bot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with It-Hub Bot.  If not, see <https://www.gnu.org/licenses/>.

*/
