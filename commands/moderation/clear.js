const Discord = require('discord.js')
const db = require('quick.db')
const ms = require('ms')

module.exports = {
  name: "clear",
  aliases: ['cl'],
  category: "moderation",
  run: async (client, message, args) => {
    if (!message.member.roles.cache.has(client.settings.modRoleId)) return message.channel.send(new Discord.MessageEmbed().setColor('#1a356f').setDescription(':x: | Vous n\'avez pas le role staff requis pour executer cela !'))

    let toDel = parseInt(args[0]);

    if (typeof toDel !== 'number' || !toDel || toDel > 100 || toDel < 1) {
      message.channel.send(
        new Discord.MessageEmbed()
          .setColor('#1a356f')
          .setDescription(':warning: | Votre nombre doit etre un entier compris entre \`1\` et \`100\`.')
      )

      return;
    }

    let messagesToDel = []

    let user = message.mentions.users.first()

    if (user) {
      let messages = await message.channel.messages.fetch({});
      messages.forEach(message => {
        if (message.author.id == user.id) messagesToDel.push(message)
      })

      if (messagesToDel.lenght > toDel) messagesToDel.lenght = toDel
    } else {
      let messages = await message.channel.messages.fetch({ limit: toDel });
      messages.forEach(message => {
        messagesToDel.push(message)
      })
    }

    await message.delete()
    message.channel.bulkDelete(messagesToDel);
    let confirm = await message.channel.send(
      new Discord.MessageEmbed()
        .setColor('#1a356f')
        .setDescription(`:white_check_mark: | J'ai bien supprimé \`${toDel.toString()}\` messages !`)
    )
    confirm.delete({timeout:5000});

    let logs = client.channels.cache.get(client.settings.logsChannelId);

    if (!logs) return;

    let tag;

    if (!user || !user.tag) tag = 'Aucun utilisateur en particulier'
    else tag = user.tag

    logs.send(
      new Discord.MessageEmbed()
        .setColor('RED')
        .setDescription(`
          **Suppression de message en masse :**

          Modérateur : \`${message.author.tag}\`
          Salon : ${message.channel}
          Nombre de messages supprimés : \`${toDel.toString()}\`
          Auteur des message : \`${tag}\``)
    )
  }
}


/*
    This file is part of It-Hub Bot.

    It-Hub Bot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    It-Hub Bot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with It-Hub Bot.  If not, see <https://www.gnu.org/licenses/>.

*/
