const Discord = require("discord.js")
const canvacord = require('canvacord')
const db = require('quick.db')

module.exports = {
  name: "rank",
  aliases: [],
  category: "info",
  run: async (client, message, args) => {
    if(args[0] === "background" || args[0] === "back" || args[0] === "image" || args[0] === "img") {
      if(message.attachments.size > 0) {
        let imgUrl = message.attachments.first().attachment
        if(!imgUrl) return message.channel.send(new Discord.MessageEmbed().setColor(client.invisible).setDescription(`${client.signs.anime_no} | ${lang.rank.noAtta}`))

        if(!imgUrl.endsWith(".png") && !imgUrl.endsWith(".jpg") && !imgUrl.endsWith(".gif")) return message.channel.send(new Discord.MessageEmbed().setColor(client.invisible).setDescription(`${client.signs.anime_no} | ${lang.rank.noImgAtta}`))

        db.set(`userconfig_${message.author.id}.rank.back`, imgUrl)

        message.channel.send(new Discord.MessageEmbed().setColor(client.invisible).setDescription(`${client.signs.anime_yes} | ${lang.rank.imgSet}`))
    } else {
        var link = args[1]
        var name = args[2]

        if(!link || !link.endsWith(".png") && !link.endsWith(".jpg") && !link.endsWith(".gif") && !link.endsWith(".jpeg") || !link.startsWith("http://") && !link.startsWith("https://")) return message.channel.send(new Discord.MessageEmbed().setColor(client.invisible).setDescription(`${client.signs.anime_no} | ${lang.rank.noUrlOrInvalid}`))

        db.set(`userconfig_${message.author.id}.rank.back`, link)

        message.channel.send(new Discord.MessageEmbed().setColor(client.invisible).setDescription(`${client.signs.anime_yes} | ${lang.rank.imgSet}`))
    }
    return;
    } else if(args[0] === "color" || args[0] === "col") {
      var color = args[1]
      if(!color) return message.channel.send(new Discord.MessageEmbed().setColor(client.invisible).setDescription(`${client.signs.anime_no} | ${lang.rank.provideColor}`))

      if(color.length !== 7 || !color.startsWith("#")) return message.channel.send(new Discord.MessageEmbed().setColor(client.invisible).setDescription(`${client.signs.anime_no} | ${lang.rank.invalidHex}`))

      db.set(`userconfig_${message.author.id}.rank.color`, color.toString().toUpperCase())

      message.channel.send(new Discord.MessageEmbed().setColor(color).setDescription(`${client.signs.anime_yes} | ${lang.rank.colorSet}`))
      return;
    }


      message.channel.startTyping()




var rankuser = message.mentions.users.first();

if(!rankuser) {
  var rankuser = message.author;
  var avatar = '1'
}

function sortByKey(array, key) {
  return array.sort(function(a, b) {
      let x = a[key]; let y = b[key];
      return ((x < y) ? 1 : ((x > y) ? -1 : 0));
  });
}

var lb = []

for (const id in db.get(`xp_${message.guild.id}`)) {
  lb.push({
  xp: db.get(`xp_${message.guild.id}.${id}.xp`),
  level: db.get(`xp_${message.guild.id}.${id}.lvl`),
  ID:id
  })
  }

  var lb = sortByKey(lb, "level")

  let i = 0
let text = ""
for (const u of lb) {
i++

if(u.ID === rankuser.id) {
var rank = i
}
}

let back = db.get(`userconfig_${rankuser.id}.rank.back`)

  var curxp = db.get(`xp_${message.guild.id}.${rankuser.id}.xp`)
  var curlvl = db.get(`xp_${message.guild.id}.${rankuser.id}.lvl`)
  var lvlxp = curxp - (curlvl*((curlvl^1.7+200)*curlvl))
  var needed = 5 * (Math.pow(curlvl, 2)) + 50 * curlvl + 100

  if(!curlvl) curlvl = "0"
  if(!rank) rank = 999
  if(!needed) needed = 999
  if(!lvlxp) lvlxp = 999
  if(!curxp) curxp = 0
    const rankCanvas = new canvacord.Rank()
      .setAvatar(rankuser.avatarURL({format: "png", dynamic: false}))
      .setCurrentXP(curxp || 0)
      .setRequiredXP(Math.floor(needed || 0))
      .setStatus(rankuser.presence.status)
      .setProgressBar("#FFFFFF", "COLOR")
      .setUsername(rankuser.username)
      .setLevel(curlvl)
      .setRank(rank)
      .setDiscriminator(rankuser.discriminator);

      let rankCard = await rankCanvas.build();
      let atta = new Discord.MessageAttachment(rankCard, 'rankCard.png')

      message.channel.send(atta)
            message.channel.stopTyping()
  }
}

/*
    This file is part of It-Hub Bot.

    It-Hub Bot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    It-Hub Bot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with It-Hub Bot.  If not, see <https://www.gnu.org/licenses/>.

*/
