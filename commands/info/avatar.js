const Discord = require("discord.js");
const {PREFIX} = require("../config.js");

module.exports = {
  name: "avatar",
  aliases: ["pp", "pdp", "photo",  "Avatar",  "Pp", "Pdp", "Photo"],
  category: "info",
  run: (client, message, args) => {

  if (!message.mentions.users.size) {

    const embed = new Discord.MessageEmbed()
    .setTimestamp()
    .setColor('1a356f')
    .setTitle('Voici votre avatar :')
    .setImage(message.author.displayAvatarURL({dynamic: true, size: 1024, format: 'png'}))
    return message.channel.send(embed)

    }


 const avatarList = message.mentions.users.map(user => {

    const embed = new Discord.MessageEmbed()
    .setTimestamp()
    .setColor('1a356f')
    .setTitle(`Voici l\'avatar de : ${user.username}`)
    .setImage(user.displayAvatarURL({dynamic: true, size: 1024, format: 'png'}))
     message.channel.send(embed)
    });
    }
}
