const Discord = require("discord.js")
const db = require("quick.db")

module.exports = {
  name: "invites",
  aliases: ["inv"],
  category: "info",
  run: async (client, message, args) => {
    let user = message.author
    if(args[0]) {
      user = message.mentions.users.first() || message.guild.members.cache.find(mbr => mbr.nickname == args[0] || mbr.user.username == args[0] || mbr.user.discriminator == args[0]).user
    }

    let vraies = db.get(`invites_${message.guild.id}.${user.id}.true`) || 0
    let fausses = db.get(`invites_${message.guild.id}.${user.id}.fake`) || 0
    let bonus = db.get(`invites_${message.guild.id}.${user.id}.bonus`) || 0
    let total = Math.round(vraies+bonus-fausses) || 0

    let embed = new Discord.MessageEmbed()
      .setTitle(`Invites | ${user.username}`)
      .setDescription(`:white_check_mark: | Vraies : ${vraies}\n:x: | Fausses : ${fausses}\n:small_red_triangle: | Bonus : ${bonus}\n:control_knobs: | Total : ${total}`)

    message.channel.send(embed)
  }
}

/*
    This file is part of It-Hub Bot.

    It-Hub Bot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    It-Hub Bot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with It-Hub Bot.  If not, see <https://www.gnu.org/licenses/>.

*/
