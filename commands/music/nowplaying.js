const Discord = require('discord.js')

module.exports = {
  name: 'nowplaying',
  aliases: ['np','nowp'],
  category: 'music',
  run: async (client, msg, args) => {
    let clientMember = msg.guild.member(client.user);

    if (!clientMember.voice.channel) return msg.channel.send(
      new Discord.MessageEmbed()
        .setColor('4e3f7b')
        .setDescription(':speaker: | Je ne suis pas entrain de jouer de musique actuellement, utilisez la commande `play` pour en ajouter une !')
    )

    let music = client.queue.get(msg.guild.id);

    if (!music) return msg.channel.send(
      new Discord.MessageEmbed()
        .setColor('4e3f7b')
        .setDescription(':speaker: | Je ne suis pas entrain de jouer de musique actuellement, utilisez la commande `play` pour en ajouter une !')
    )

    let currentMusic = music.songs[0];

    if (!currentMusic) return msg.channel.send(
      new Discord.MessageEmbed()
        .setColor('4e3f7b')
        .setDescription(':speaker: | Je ne suis pas entrain de jouer de musique actuellement, utilisez la commande `play` pour en ajouter une !')
    )

    msg.channel.send(
      new Discord.MessageEmbed()
        .setColor('1a356f')
        .setTitle(`${currentMusic.title} - Par ${currentMusic.author.name || 'Inconnu'}`)
        .setURL(currentMusic.url)
        .setThumbnail(currentMusic.bestThumbnail.url)
        .setDescription(`
          Écoutes : ${currentMusic.views.toLocaleString('fr-FR')}
          Date de création : ${currentMusic.uploadedAt || 'Inconnue'}
          Durée : ${currentMusic.duration}
          Demandé par : \`${currentMusic.addedBy.tag}\``)
    )
  }
}
