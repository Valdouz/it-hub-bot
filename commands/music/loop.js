const Discord = require('discord.js')

module.exports = {
  name: 'loop',
  aliases: [],
  category: 'music',
  run: async (client, message, args) => {
    let clientMember = message.guild.member(client.user);

    if (!clientMember.voice.channel) return message.channel.send(
      new Discord.MessageEmbed()
        .setColor('4e3f7b')
        .setDescription(':speaker: | Je ne suis pas entrain de jouer de musique actuellement, utilisez la commande `play` pour en ajouter une !')
    )

    if (!message.member.voice.channel || message.member.voice.channel !== clientMember.voice.channel) return msg.channel.send(
      new Discord.MessageEmbed()
        .setColor('4e3f7b')
        .setDescription(':mag: | Vous n\'êtes pas dans le salon vocal musical. Vous ne pouvez pas utiliser cette commande !')
    )

    let guildQueue = client.queue.get(message.guild.id);

    if (guildQueue.loop) {
      guildQueue.loop = false
      message.channel.send(
        new Discord.MessageEmbed()
          .setColor('1a356f')
          .setDescription(':arrow_forward: | La liste ne se répètera plus ! Pour réactiver l\'option, réutilisez la commande `loop`')
      )
    } else {
      guildQueue.loop = true
      message.channel.send(
        new Discord.MessageEmbed()
          .setColor('1a356f')
          .setDescription(':repeat: | La liste va se jouer en boucle a partir de maintenant ! Pour désactiver l\'option utilisez la commande `loop`')
      )
    }
  }
}
