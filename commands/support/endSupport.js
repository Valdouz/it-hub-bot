const Discord = require("discord.js")
const db = require('quick.db')

module.exports = {
  name: "endsupport",
  aliases: ["end"],
  category: "support",
  run: async (client, msg, args) => {
    if(!msg.channel.topic.startsWith(`<@${msg.author.id}>`)) return msg.channel.send(':x: | Vous ne pouvez effectuer cette commande que dans un salon de support vous appartenant !')

    let valid = true

    let message = await msg.channel.send(new Discord.MessageEmbed().setColor('RED').setDescription(':warning: | Vous êtes sur le point de supprimer ce salon. Êtes vous sûr d\'avoir résolu votre problème ?\nSi oui cliquez sur :white_check_mark: sous ce message, sinon ne faites rien.'))

    message.react('✅')

    let filter = (reaction, user) => {
      return user.id === msg.author.id && reaction.emoji.name == '✅'
    }

    let collector = message.createReactionCollector(filter, {})

    collector.once('collect', (reaction, user) => {
      if(user.bot) return;

      db.set(`supports.${msg.author.id}`, null)

      msg.channel.delete()
    })

  }
}

/*
    This file is part of It-Hub Bot.

    It-Hub Bot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    It-Hub Bot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with It-Hub Bot.  If not, see <https://www.gnu.org/licenses/>.

*/
